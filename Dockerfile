FROM node:14.16.1-alpine as build-step
ARG ENVIROMENT
RUN mkdir -p /app
WORKDIR /app
COPY . /app
RUN npm install 
RUN yarn install && yarn run build

# RUN npm i npm-install-all
# RUN npm install -g yarn --force
# RUN yarn install && yarn run build

FROM nginx:latest as proxy
RUN rm /usr/share/nginx/html/*
COPY --from=build-step /app/build/resources/main/static/ /usr/share/nginx/html
EXPOSE 80